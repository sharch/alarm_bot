from time_class import Time
import nltk
import re


from nltk import pos_tag, word_tokenize

from word2number import w2n


types_of_sentence = {0:'[unknown]', 1:'[tStamp]', 2:'[tPeriod]', 3:'[tSchedule]'}

def parse_sentence(input):
    pos_tagged = nltk.pos_tag(word_tokenize(input))



    sentence_type = 1
    important_words = []
    timevalue = 0

    for pos in pos_tagged:
        val = pos[0].lower()
        key = pos[1]

        time = Time()

        if(key == 'CD'):
            if(val.isdigit()):
                timevalue = int(val)
            else:
                timevalue = w2n.word_to_num(val)

        if(key == 'NN' or key == 'NNS'):
            if(val[:4] == 'year' or val[:5] == 'month' or val[:4] == 'week'):
                important_words += [val]
                sentence_type = 2
            if(val == 'minutes'):
                important_words += [val]

        if(key == 'JJ'):
            if(val == 'next' or val == 'last'):
                important_words += [val]
                sentence_type = 2

        if(key == 'IN'):
            if(val == 'before' or val == 'after'):
                important_words += [val]
                sentence_type = 3

        if(val in ['hour', 'hours', 'minutes', 'minute', 'morning', 'evening']):
            important_words += [val]

    #print(important_words)


    if(sentence_type == 3):
        time.inc_date()
        #print(timevalue)
        time.set_hours(timevalue)
        time.set_min(0)
        if('morning' in important_words):
            if(time.get_ihours() > 12): time.update_hours(-12)
        elif('evening' in important_words):
            if(time.get_ihours() < 12): time.update_hours(12)
        print(types_of_sentence[sentence_type], time.get_human())

    if(sentence_type == 2):
        delta = 0
        if('next' in important_words): delta = 1
        elif('last' in important_words): delta = -1

        print(types_of_sentence[sentence_type], time.get_year(), time.get_iyear() + delta*timevalue)


    if(sentence_type == 1):
        if('hour' in important_words or 'hours' in important_words):
            time.update_hours(timevalue)
        elif('minute' in important_words or 'minutes' in important_words):
            time.update_min(timevalue)

        if('morning' in important_words):
            if(time.get_ihours() > 12): time.update_hours(-12)
        elif('evening' in important_words):
            if(time.get_ihours() < 12): time.update_hours(12)
        print(types_of_sentence[sentence_type], time.get_human())








examples = ["Wake me up 20 minutes from now", "I was in Mumbai for last two years", "Any time after 2 is fine"]


for e in examples:
    print(e)
    parse_sentence(e)
    print('==\n')
parse_sentence(input())
print('==\n')



'''
1. Wake me up 20 minutes from now - [tStamp]2020 hrs, Thursday, July 20th
2. I was in Mumbai for last two years - [tPeriod] - 2013-2015
3. Any timer after 2 is fine - [tSchedule] - start - 0200 hrs,July 21st 2015

'''

