##  Alarm-Bot
Work in progress.

### OBJECTIVE ###
To build an alarm bot that processes text, figures the relative/ absolute time being spoken about and sets an alarm accordingly. 
The python script currently returns the time stamp or schedule through the use of regular expressions. 

### Dependencies ###
nltk, word2number

### Example ### 
### **Current Results** ###
1. Wake me up 20 minutes from now - [tStamp]2020 hrs, Thursday, July 20th
2. I was in Mumbai for last two years - [tPeriod] - 2013-2015
3. Any timer after 2 is fine - [tSchedule] - start - 0200 hrs,July 21st 2015